
#include <stdint.h>

extern volatile uint32_t regs[];

static inline uint32_t readl(unsigned long reg)
{
	return regs[reg / 4];
}
static inline void writel(uint32_t val, unsigned long reg)
{
	regs[reg / 4] = val;
}
