
#include <stdint.h>
#include "io.h"
#include "hardware.h"
#include "gpio.h"

int gpio_config(int gpio_num, int mode_ctl, int remap)
{
	uint32_t base, v, msk;
	int port = gpio_num >> 4;
	int pin = gpio_num & 0xf;
	int shift;

	if (gpio_num < 0 || gpio_num > 16*5)
		return -1;
	
	base = GPIO_BASE + port * 0x400 + ((pin > 7) ? 0x04 : 0x00);
	v = readl(base);
	/* 4 bits per pin */
	shift = (pin >= 8 ? pin - 8 : pin) << 2;
	msk = 0xf << shift;
	v &= ~msk;
	v |= (mode_ctl << shift);
	writel(v, base);

	writel(remap, AGIO_PCF0);
	return 0;
}

int gpio_set(int gpio_num, int v)
{
	uint32_t base, _v;
	int port = gpio_num >> 4;
	int pin = gpio_num & 0xf;

	if (gpio_num < 0 || gpio_num > 16*5)
		return -1;

	base = GPIO_BASE + port * 0x40c;
	_v = readl(base);
	if (v)
		_v |= (1 << pin);
	else
		_v &= ~(1 << pin);
	writel(_v, base);
	return 0;
}
