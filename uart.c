
#include <stdint.h>
#include "io.h"
#include "uart.h"
#include "gpio.h"

#define USART_SR		0x0
# define TXE			BIT(7)
# define TC			BIT(6)
# define RXNE			BIT(5)
# define ORE			BIT(3)
# define FE			BIT(1)

#define USART_DR		0x4
#define USART_BRR		0x8
#define USART_CR1		0xc
# define OVER8			BIT(15)
# define UE			BIT(13)
# define M			BIT(12)
# define PCE			BIT(10)
# define PS			BIT(9)
# define PEIE			BIT(8)
# define TXEIE			BIT(7)
# define TCIE			BIT(6)
# define RXNEIE			BIT(5)
# define TE			BIT(3)
# define RE			BIT(2)

#define USART_CR2		0x10
#define USART_CR3		0x14
#define USART_GTPR		0x18

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(a) (sizeof(a) / sizeof(a[0]))
#endif


struct uart_platform_data {
	void *base;
	unsigned long clock;
	unsigned int gpio_periph_clk_reg;
	unsigned int gpio_periph_clk_mask;
	unsigned int periph_clk_reg;
	unsigned int periph_clk_mask;
	int rx_pin;
	int tx_pin;
	int remap;
};


struct uart_platform_data uarts[] = {
	[0] = {
		.base = (void *)0x40013800,
		.clock = APB2_CLK,
		.gpio_periph_clk_reg = RCU_APB2EN,
		/* Also enable AFEN ? */
		.gpio_periph_clk_mask = ((3 << 2) | (1 << 0)),
		.periph_clk_reg = RCU_APB2EN,
		.periph_clk_mask = (1 << 14),
		.tx_pin = 9,
		.rx_pin = 10,
		.remap = 0,
	},
	[1] = {
		.base = (void *)0x40004400,
		.clock = APB1_CLK,
		.periph_clk_reg = RCU_APB1EN,
		.periph_clk_mask = (1 << 17),
		.tx_pin = -1,
		.rx_pin = -1,
	},
	[2] = {
		.base = (void *)0x40004800,
		.clock = APB1_CLK,
		.periph_clk_reg = RCU_APB1EN,
		.periph_clk_mask = (1 << 18),
		.tx_pin = -1,
		.rx_pin = -1,
	},
	[3] = {
		.base = (void *)0x40004c00,
		.clock = APB1_CLK,
		.periph_clk_reg = RCU_APB1EN,
		.periph_clk_mask = (1 << 19),
		.tx_pin = -1,
		.rx_pin = -1,
	},
	[4] = {
		.base = (void *)0x40005000,
		.clock = APB1_CLK,
		.periph_clk_reg = RCU_APB1EN,
		.periph_clk_mask = (1 << 20),
		.tx_pin = -1,
		.rx_pin = -1,
	},
};

static void uart_set_baudrate(struct uart_platform_data *plat, uint32_t baud)
{
	uint32_t base = (uint32_t)plat->base;
	uint32_t tmp;
	uint32_t div_fraction, div_mantissa;

	/* 16 samples per bit */
	tmp = (plat->clock >> 4) * 100 / baud;
	div_fraction = ((tmp % 100) << 4) / 100;
	div_mantissa = tmp / 100;
	writel(div_fraction | (div_mantissa << 4), base + USART_BRR);
}

static int uart_hw_init(int index, uint32_t baud)
{
	struct uart_platform_data *plat = &uarts[index];
	uint32_t base = (uint32_t)plat->base;
	uint32_t v;

	/* Enable corresponding gpio port peripheral clock */
	v = readl(plat->gpio_periph_clk_reg);
	v |= plat->gpio_periph_clk_mask;
	writel(v, plat->gpio_periph_clk_reg);
	/* Enable peripheral clock */
	v = readl(plat->periph_clk_reg);
	v |= plat->periph_clk_mask;
	writel(v, plat->periph_clk_reg);
	/* Setup rx and rx pins */
	if (plat->tx_pin >= 0)
		gpio_config(plat->tx_pin, GPIO_CTL_AF_PP|GPIO_MODE_OUT_50M,
			    plat->remap);
	if (plat->rx_pin >= 0)
		gpio_config(plat->rx_pin, GPIO_CTL_IN_FLOAT|GPIO_MODE_INPUT,
			    plat->remap);
	/* Setup baud rate generator */
	uart_set_baudrate(plat, 115200);
	/* Enable uart */
	writel(UE|RE|TE, base + USART_CR1);
	return index;
}

int uart_open(const char *ptr, uint32_t baud)
{
	/* "usartX" */
	int index;

	if (ptr[1] == 's')
		/* "usart0" */
		index = ptr[5] - 0x30;
	else {
		/* "uartX" */
		index = ptr[4] - 0x30;
	}

	if (index < 0 || index >= ARRAY_SIZE(uarts))
		return -1;
	
	return uart_hw_init(index, baud);
}

int uart_putchar(int index, int c)
{
	struct uart_platform_data *plat = &uarts[index];
	uint32_t base = (uint32_t)plat->base;

	while (!(readl(base + USART_SR) & TXE));
	writel(c, base + USART_DR);
	return 1;
}
