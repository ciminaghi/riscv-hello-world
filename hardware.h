
#define BIT(a) ((1) << a)

#define RCU_BASE  0x40021000

#define RCU_CTL RCU_BASE
# define IRC8MEN  (1<<0)
# define HXTALEN  (1<<16)
# define HXTALSTB (1<<17)
# define HXTALBPS (1<<18)
# define CKMEN    (1<<19)
# define PLLEN    (1<<24)
# define PLLSTB   (1<<25)
# define PLL1EN   (1<<26)
# define PLL2EN   (1<<28)

#define RCU_CFG0 (RCU_BASE + 0x4)
# define SCS_MSK  (3)
# define SCS_CK_IRC8M (0)
# define SCS_CK_HXTAL (1)
# define SCS_CK_PLL   (2)
# define SCSS_MSK (3 << 2)
# define SCSS_CK_PLL (2 << 2)
# define AHBPSC_MSK   (0xf << 4)
# define AHB_CKSYS_DIV1 0
# define APB1PSC_MSK  (0x7 << 8)
# define APB1_CKAHB_DIV2 (4 << 8)
# define APB2PSC_MSK  (0x7 << 11)
# define APB2_CKAHB_DIV1 0
# define ADCPSC_MSK   (0x7 << 14)
# define PLLSEL       (1 << 16)
# define PLLSRC_HXTAL PLLSEL
# define PREDV0_LSB   (1 << 17)
# define PLLMF_MSK    (0xf << 18)
# define USBFSPSC_MSK (0x3 << 22)
# define CKOUTSEL_MSK (0xf << 24)
# define ADCPSC_MSK_MSB (1 << 28)
# define PLLMF_4      (1 << 29)
# define PLL_MUL24    (PLLMF_4 | (23UL << 18))

#define RCU_INT (RCU_BASE + 0x08)

#define RCU_CFG1 (RCU_BASE + 0x2c)
# define PREDV0SEL    (1 << 16)
# define PREDV1_MSK   (0xf << 4)
# define PLL1MF_MSK   (0xf << 8)
# define PREDV0_MSK   (0xf)
# define PREDV0SRC_HXTAL 0
# define PREDV0_DIV2  1

#define RCU_AHBEN  (RCU_BASE + 0x14)
#define RCU_APB2EN (RCU_BASE  + 0x18)
#define RCU_APB1EN (RCU_BASE + 0x1c)


#define SYS_CLK 96000000

#define APB1_CLK (SYS_CLK/2)
#define APB2_CLK (SYS_CLK)
#define AHB_CLK  (SYS_CLK)

extern int setup(void);

#define AFIO_BASE 0x40010000
#define AGIO_PCF0 (AFIO_BASE + 0x04)
#define GPIO_BASE 0x40010800

#define GPIO_MODE_INPUT		0
#define GPIO_MODE_OUT_10M	1
#define GPIO_MODE_OUT_2M	2
#define GPIO_MODE_OUT_50M	3

#define GPIO_CTL_IN_ANALOG	(0 << 2)
#define GPIO_CTL_OUT_PP		(0 << 2)
#define GPIO_CTL_IN_FLOAT	(1 << 2)
#define GPIO_CTL_OUT_OD		(1 << 2)
#define GPIO_CTL_IN_PU_PD	(2 << 2)
#define GPIO_CTL_AF_PP		(2 << 2)
#define GPIO_CTL_AF_OD		(3 << 2)

#define SPI0_REMAP BIT(0)
#define I2C0_REMAP BIT(1)
#define USART0_REMAP BIT(2)
#define USART1_REMAP BIT(3)
#define USART2_REMAP_PART BIT(4)
#define USART2_REMAP_FULL (BIT(4)|BIT(5))
#define TIMER0_REMAP_PART BIT(6)
#define TIMER0_REMAP_FILL (BIT(6)|BIT(7))
