#ifndef __TIMER_BUMBLEBEE_H__
#define __TIMER_BUMBLEBEE_H__

#include <stdint.h>

#include "hardware.h"

/* Defined in linker script */
extern volatile uint32_t jiffies_ptr[0];
extern volatile uint64_t jiffies64_ptr[0];

#define jiffies (jiffies_ptr[0])
#define jiffies64 (jiffies64_ptr[0])

#define TIMER_FREQ ((unsigned long long)(SYS_CLK/4))

/*
 * (gdb) p (unsigned long long)256*1000000000/24000000
 * $8 = 10666
 * (gdb) p ((unsigned long long)24000000*10666)>>8
 * $9 = 999937500
 */
static inline unsigned long long counter_to_ns(uint64_t v)
{
	return (v * 10666ULL) >> 8;
}

/*
 * (gdb) p ((unsigned long long)4096*1000000)/24000000
 * $14 = 170
 * (gdb) p ((unsigned long long)24000000*170)>>12
 * $16 = 996093
 */
static inline unsigned long counter_to_us(uint64_t v)
{
	return (v * 170ULL) >> 12;
}

#endif /* __TIMER_BUMBLEBEE_H__ */
