
#ifndef __GPIO_H__
#define __GPIO_H__

extern int gpio_config(int gpio_num, int mode_ctl, int remap);
extern int gpio_set(int gpio_num, int v);


#endif /* __GPIO_H__ */
