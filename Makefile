

CROSS_COMPILE:=/home/develop/riscv/toolchain/bin/riscv-nuclei-elf-
CC:=$(CROSS_COMPILE)gcc
OBJCOPY:=$(CROSS_COMPILE)objcopy
CFLAGS:=-Wall --no-builtin-putc -g -mabi=ilp32
ASFLAGS:=-mabi=ilp32

all: hello-world.bin

hello-world.bin: hello-world
	$(OBJCOPY) -O binary $< $@

hello-world: main.o setup.o gpio.o uart.o start.o
	$(CC) $(CFLAGS) -Ttarget.lds -nostdlib -nostartfiles -Wl,-Map=$@.map -o $@ $+

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f hello-world hello-world.bin *.o *~ *.map

.PHONY: all
