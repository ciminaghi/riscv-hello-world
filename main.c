#include "io.h"
#include "hardware.h"
#include "uart.h"
#include "gpio.h"
#include "timer-bumblebee.h"

static int uart_index = -1;

int putc(int c)
{
	return uart_putchar(uart_index, c);
}

int puts(const char *s)
{
	int i;

	for (i = 0; s[i]; i++)
		putc(s[i]);
	return i;
}

static int print_unsigned(char *out, unsigned int n)
{
	unsigned long div = 1000000000ULL;
	int i;

	i = 0;
	while (div) {
		out[i++] = (n / div) + '0';
		n = n % div;
		div /= 10;
	}
	return i;
}

int main()
{
	uart_index = uart_open("usart0", 115200);
	static char buf[80];
	uint32_t prev_rem, rem;
	int v = 0;
	
	gpio_config(16, GPIO_CTL_OUT_PP|GPIO_MODE_OUT_10M, 0);
	gpio_set(16, v);

	if (uart_index < 0)
		return -1;

	puts("\r\nHello world !\r\n");
	while(1) {
		rem = counter_to_us(jiffies64) % 1000000;
		if (prev_rem > rem) {
			v ^= 1;
			gpio_set(16, v);
			print_unsigned(buf, counter_to_us(jiffies64));
			puts(buf);
			putc('\r'); putc('\n');
		}
		prev_rem = rem;
	}
	return 0;
}
