#include <stdint.h>
#include "io.h"
#include "hardware.h"

#define HXTAL_STARTUP_TIMEOUT (unsigned int)0xffff

#if SYS_CLK != 96000000
# error "works with 96MHz only"
#endif

static int system_clock_96m_hxtal(void)
{
	unsigned int timeout = 0U;
	uint32_t v;

	/* enable HXTAL */
	v = readl(RCU_CTL);
	v |= HXTALEN;
	writel(v, RCU_CTL);

	/*
	 * wait until HXTAL is stable or the startup time is
	 * longer than HXTAL_STARTUP_TIMEOUT
	 */
	do{
		v = readl(RCU_CTL);
	} while(!(v & HXTALSTB) &&
		(timeout++ != HXTAL_STARTUP_TIMEOUT));

	/* if fail */
	if(!(v & HXTALSTB))
		return -1;

	/* HXTAL is stable */
	v = readl(RCU_CFG0);
	/* AHB = SYSCLK */
	v |= AHB_CKSYS_DIV1;
	/* APB2 = AHB/1 */
	v |= APB2_CKAHB_DIV1;
	/* APB1 = AHB/2 */
	v |= APB1_CKAHB_DIV2;
	writel(v, RCU_CFG0);

	/* External clock 8MHz */
	/* CK_PLL = (CK_PREDIV0) * 24 = 96 MHz */
	v &= ~(PLLMF_MSK | PLLMF_4);
	v |= (PLLSRC_HXTAL | PLL_MUL24);
	writel(v, RCU_CFG0);

	v = readl(RCU_CFG1);
	v &= ~(PREDV0SEL | PREDV1_MSK | PLL1MF_MSK | PREDV0_MSK);
	v |= (PREDV0SRC_HXTAL | PREDV0_DIV2);
	writel(v, RCU_CFG1);

	/* enable PLL */
	v = readl(RCU_CTL);
	v |= PLLEN;
	writel(v, RCU_CTL);

	/* wait until PLL is stable */
	do {
		v = readl(RCU_CTL);
	} while(!(v & PLLSTB));

	/* select PLL as system clock */
	v = readl(RCU_CFG0);
	v &= ~SCS_MSK;
	v |= SCS_CK_PLL;
	writel(v, RCU_CFG0);

	/* wait until PLL is selected as system clock */
	do {
		v = readl(RCU_CFG0);
	} while(!(v & SCSS_CK_PLL));
	return 0;
}


int setup(void)
{
	uint32_t v;
	
	/* reset the RCC clock configuration to the default reset state */
	/* enable IRC8M */
	v = readl(RCU_CTL);
	v |= IRC8MEN;
	writel(v, RCU_CTL);

	/* reset SCS, AHBPSC, APB1PSC, APB2PSC, ADCPSC, CKOUT0SEL bits */
	v = readl(RCU_CFG0);
	v &= ~(SCS_MSK | AHBPSC_MSK | APB1PSC_MSK | APB2PSC_MSK |
	       ADCPSC_MSK | ADCPSC_MSK_MSB | CKOUTSEL_MSK);
	writel(v, RCU_CFG0);

	/* reset HXTALEN, CKMEN, PLLEN bits */
	v = readl(RCU_CTL);
	v &= ~(HXTALEN | CKMEN | PLLEN);
	writel(v, RCU_CTL);

	/* Reset HXTALBPS bit */
	v &= ~HXTALBPS;
	writel(v, RCU_CTL);

	/* reset PLLSEL, PREDV0_LSB, PLLMF, USBFSPSC bits */
	v = readl(RCU_CFG0);
	v &= ~(PLLSEL | PREDV0_LSB | PLLMF_MSK | USBFSPSC_MSK | PLLMF_4);
	writel(v, RCU_CFG0);
	writel(0, RCU_CFG1);

	/* Reset HXTALEN, CKMEN, PLLEN, PLL1EN and PLL2EN bits */
	v = readl(RCU_CTL);
	v &= ~(PLLEN | PLL1EN | PLL2EN | CKMEN | HXTALEN);
	writel(v, RCU_CTL);

	/* disable all interrupts */
	writel(0x00FF0000U, RCU_INT);

	return system_clock_96m_hxtal();
}

